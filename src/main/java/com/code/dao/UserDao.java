package com.code.dao;

import java.util.List;

import com.code.po.User;


public interface UserDao {

	public User findUserById(int id) throws Exception;

	public List<User> findUsersByName(String name) throws Exception;

	public void insertUser(User user) throws Exception;

}
