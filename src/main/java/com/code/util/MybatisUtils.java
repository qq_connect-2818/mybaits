package com.code.util;

import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class MybatisUtils {

	private static SqlSessionFactory sqlSessionFactory;

	private static ThreadLocal<SqlSession> tl = new ThreadLocal<SqlSession>();

	synchronized public static SqlSessionFactory getSqlSessionFactory() {

		try {
			if (sqlSessionFactory == null) {
				String resource = "SqlMapConfig.xml";
				InputStream in = Resources.getResourceAsStream(resource);
				 sqlSessionFactory = new SqlSessionFactoryBuilder()
						.build(in);
			}

		} catch (Exception e) {

		}
		return sqlSessionFactory;

	}

	public static SqlSession getSqlSession() {
		SqlSession sqlSession = tl.get();
		if (sqlSession == null) {
			sqlSession = getSqlSessionFactory().openSession();
			tl.set(sqlSession);
		}
		return sqlSession;
	}
	
	public static void commit(){
		if(tl.get()!=null){
			tl.get().commit();
			tl.get().close();
			tl.set(null);
		}
	}
	
	public static void rollback(){
		if(tl.get()!=null){
			tl.get().commit();
			tl.get().rollback();
			tl.set(null);
		}
	}
	

}
